(function() {
	'use strict';

	angular
		.module('ddn_installation')
		.config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider, $urlRouterProvider) {
		$stateProvider
		.state('login', {
			url: '/login',
			cache: false,
			templateUrl: 'app/partials/login.html',
			controller: 'LoginController'
		})
		.state('home', {
			url: '/home',
			templateUrl: 'app/partials/home/home.html',			
			controller: 'HomeController',
			resolve:{
				status: function(AuthService){
					return AuthService.getStatus();
				}
			}
		})
		.state('issue', {
			url: '/issues',
			templateUrl: 'app/partials/ddn/issue.html',			
			controller: 'IssueController',
			resolve:{
				status: function(AuthService){
					return AuthService.getStatus();
				}
			}
		})
		.state('wardTable', {
			url: '/wards',
			templateUrl: 'app/partials/ward/ward.html',
			controller: 'wardController',
			resolve:{
				status: function(AuthService){
					return AuthService.getStatus();
				}
			}
		})
		.state('show_map', {
			url: '/show_map/:ward',
			templateUrl:'app/partials/map/map.html',
			controller:'showMapController',

			resolve:{
				status: function(AuthService){
					return AuthService.getStatus();
				}
			}
		});
		$urlRouterProvider.otherwise('/login');
	}
})();
