angular.module('ddn_installation')
.controller('NavController', function ($rootScope, $scope, $state, ZipprAuthService, localStorageService) {
'use strict';

  $scope.isCollapsed = true;
  $scope.logout =  function(){
    ZipprAuthService.logout().then(function(){
      $rootScope.verifiedType = false;
      $rootScope.showAPReport = false;
      $rootScope.isAdminUser = false;
      clearLocalStorage();
    },
    function(error){
      console.log(error);
      clearLocalStorage();
    });
	};

  $scope.getUserName = function(){
    return localStorageService.get('name') || 'Profile';
  };

  function clearLocalStorage(){
    $scope.isCollapsed = true;
    localStorageService.remove('Id');
    localStorageService.remove('userName');
    localStorageService.remove('sessiontoken');
    localStorageService.remove('apikey');
    localStorageService.remove('clientobjectid');
    localStorageService.remove('client');
    localStorageService.remove('name');
    localStorageService.clearAll();
    $state.go('login');
  }
});