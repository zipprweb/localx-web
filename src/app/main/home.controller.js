angular.module('ddn_installation')
.controller('HomeController', function ($rootScope, logger, $scope, $state, $http, ZipprAPI ,uiGridConstants,$uibModal,localStorageService) {
	'use strict';
	$scope.init = function (argument) {
		$scope.workOrder = "AP"
		$scope.prefixCodes = []
		$scope.projects = []
		$scope.wards = [] 
		$scope.isSelectOpen = true;
		$scope.isSelectedPrefixCode = true;
		$scope.errorCodes = ["Junk Data", "Improper Data", "Valid but irrelavent"]
		$scope.selectedErrorCode = $scope.errorCodes[0]
		$scope.selectedWard = ''
		$scope.selectedPrefixCode = ''
		$scope.getListOfProjects();
	}
	$scope.getListOfProjects = function() {
		console.log("getting projects")
		ZipprAPI.getListOfProjects.get({work_order: $scope.workOrder}, function(data) {
			if(data.ok === true){

				var projects=[]
				$scope.projectsList = data.response.projects;
				for (var i = 0; i < $scope.projectsList.length; i++) {
					projects.push($scope.projectsList[i].prefix_code)
				}
				projects.sort(sortArray);
				$scope.projects = projects
				$scope.selectedPrefixCode = projects[0]
				$scope.getListOfWards();
			} else {
				$scope.prefixCodes = []
				$scope.isSelectedPrefixCode = false
			}
		}, function(e){
			$scope.prefixCodes = []
			$scope.isSelectedPrefixCode = false
		});
	}
	$scope.getListOfWards = function () {
		if($scope.selectedPrefixCode){
			let query = {
				'work_order': $scope.workOrder,
				'prefix_code': $scope.selectedPrefixCode
			}
			console.log(query)
			ZipprAPI.getListOfWardsLocX.get(query, function(data, err) {
				if(data.ok === true && data.response.wards.length){
					var wardsList = []
					var wardsListObj = data.response.wards
					for (var i = 0; i < wardsListObj.length; i++) {
						var singleWard = Number(wardsListObj[i].ward)
						wardsList.push(singleWard)
					}
					wardsList.sort(sortArray)
					wardsList = wardsList.filter(onlyUnique)
					$scope.selectedWard = wardsList[0]
					$scope.wards = wardsList
				}
				else {
					$scope.wards = []
				}
			}, function(e){
				console.log(e);
				$scope.wards = []
			});
		}
		else{
			$scope.wards = []
			$scope.selectedWard = null
		}
	}

	$scope.deleteLocalXData = function () {
		if($scope.selectedPrefixCode && $scope.selectedWard && $scope.username && $scope.errorValue && $scope.selectedErrorCode){
			let query = {
				"work_order": $scope.workOrder,
				"prefix_code" : $scope.selectedPrefixCode,
				"ward" : $scope.selectedWard,
				"local_x_updated_by": $scope.local_x_updated_by,
				"error_name" : $scope.selectedErrorCode,
				"locality_name" : $scope.locality_name
			}
			
			ZipprAPI.deleteLocalXData.delete(query,function (data,err) {
				if(data.ok === true){
					logger.logSuccess('Deleted the data successfully');
					$scope.locality_name = null
					$scope.local_x_updated_by = null
				}
				else{
					logger.logError('Error: '+ data.error.reason);
				}
			});
		}
		else{
			logger.logError('Enter the data');
		}
	}

	function sortArray(a, b) {
		return a > b ? 1 : b > a ? -1 : 0;
	}
	function onlyUnique(value, index, self) { 
    	return self.indexOf(value) === index;
	}
	$scope.init();
})
.directive('uppercaseOnly', [
  function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        element.on('keypress', function(e) {
          var char = e.char || String.fromCharCode(e.charCode);
     
        });

        function parser(value) {
          if (ctrl.$isEmpty(value)) {
            return value;
          }
          var formatedValue = value.toUpperCase();
          if (ctrl.$viewValue !== formatedValue) {
            ctrl.$setViewValue(formatedValue);
            ctrl.$render();
          }
          return formatedValue;
        }

        function formatter(value) {
          if (ctrl.$isEmpty(value)) {
            return value;
          }
          return value.toUpperCase();
        }

        ctrl.$formatters.push(formatter);
        ctrl.$parsers.push(parser);
      }
    };
  }
])
