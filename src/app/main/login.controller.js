angular.module('ddn_installation')
.controller('LoginController',function ($rootScope, $scope, $state, $http, ZipprAuthService, $log, localStorageService, logger) {
	'use strict';
	$rootScope.verifiedType = false;
	$scope.loading = false;

	if(localStorageService.get('x-zippr-sessiontoken')){
		if(localStorageService.get('user_type') === 'admin') {
		 $rootScope.isAdminUser = true;
		}
		$state.go('home');
	}

	$scope.login =  function(){
		console.log('Hello I am Here....')
		if(!$scope.userName || !$scope.password){
			return;
		}

		$scope.errorMsg = '';
		$scope.loading = true;

		ZipprAuthService.login($scope.userName, $scope.password)
			.then(function(data){
				$scope.loading = false;
				if (data.response.user.user_type === 'admin' || data.response.user.user_type === 'client') {
					localStorageService.set('name', data.response.user.name);
					localStorageService.set('user_type', data.response.user.user_type);
					localStorageService.set('x-zippr-sessiontoken', data.response['x-zippr-sessiontoken']);
					$http.defaults.headers.common['x-zippr-sessiontoken'] = data.response['x-zippr-sessiontoken'];
					$rootScope.isAdminUser = data.response.user.user_type === 'admin' ? true: false;
					$rootScope.verifiedType = true;
					logger.logSuccess('Logged in successfully.');
					$state.go('home');
				} else {
					$scope.errorMsg = 'You do not have permission to login!!';
				}
			}, function(error){
				$scope.password = '';
				$scope.loading = false;
				$scope.errorMsg = (error && error.error && error.error.reason) ? error.error.reason : 'Invalid Username or Password.';
				$log.debug(error,'error');
			});
		};
});