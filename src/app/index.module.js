(function() {
	'use strict';

	angular .module('ddn_installation', [
		'ngCookies',
		'ngTouch',
		'ngResource',
		'ngSanitize',
		'ui.bootstrap',
		'ui.router',
		'zippr.config',
		'zippr.auth',
		'zippr.api',
		'LocalStorageModule',
		'app.ui.services',
		'ngAnimate',
		'ui.grid',
		'ui.grid.pagination',
		'ngMaterial',
		'ngMessages',
		'ngMaterialDateRangePicker',
		'720kb.datepicker',
		'ngTable'

		]);
})();