(function() {
	'use strict';

	angular
		.module('ddn_installation')
		.run(runBlock);

	/** @ngInject */
	function runBlock($rootScope, $state, $http, localStorageService) {
		$http.defaults.headers.common['x-zippr-sessiontoken'] = (localStorageService.get("sessiontoken") || '').toString();
		$http.defaults.headers.common['x-zippr-api-key'] = (localStorageService.get("apikey") || '').toString();
		
		if(localStorageService.get("apikey") !== undefined){
			$rootScope.verifiedType = true;
		}

		$rootScope.isAdminUser = (localStorageService.get('user_type') === 'admin');

		$rootScope.$on('$stateChangeError',  function(event, toState, toParams, fromState, fromParams, error){ 
			$state.go("login");
		});
	}
	
})();
