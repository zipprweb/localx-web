(function() {
  'use strict';

  angular
    .module('ddn_installation')
    .constant('USER_ROLES', {
      admin: 'admin',
      public: 'public'
    })
    .config(config);

  /** @ngInject */
  function config($logProvider, localStorageServiceProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
    localStorageServiceProvider.setPrefix('zp_road_network').setStorageType('sessionStorage').setNotify(true, true);
  }
})();