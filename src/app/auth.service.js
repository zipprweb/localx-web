angular.module('ddn_installation')
	.service('AuthService', function($q, $rootScope, $state, localStorageService) {
	'use strict';

	return {
		getStatus: function() {
			var dfd = $q.defer();
			var name = localStorageService.get('name');
			if(name !== '' && name !==null) {
				dfd.resolve({
					name: 'Success'
				});
				$rootScope.verifiedType = true;
				$rootScope.isAdminUser = localStorageService.get('user_type') === 'admin' ? true: false;
			} else {
				dfd.reject();
				$state.go('login');
			}

			return dfd.promise;
		}
	};
});